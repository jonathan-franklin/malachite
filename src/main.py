# main.py
#
# Copyright 2020 Jonathan Franklin
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
import gi
import os

gi.require_version('Gtk', '4.0')

from gi.repository import Gtk, Gio

from .window import MalachiteWindow


class Application(Gtk.Application):
    def __init__(self):
        super().__init__(application_id='com.gitlab.jonathan_franklin.Malachite',
                         flags=Gio.ApplicationFlags.FLAGS_NONE)


    def do_activate(self):
        win = self.props.active_window
        if not win:
            win = MalachiteWindow(application=self)

        handlers = {
            #"onDestroy": gtk.main_quit,
            "onButtonPressed": self.renamer
        }

        self.builder = Gtk.Builder()
        self.builder.add_from_resource('/com/gitlab/jonathan_franklin/Malachite/MalachiteUI.ui')
        self.builder.connect_signals(handlers)
        self.fileChooser = self.builder.get_object("file_chooser")

        self.window = self.builder.get_object("MalachiteWindow")
        self.window.show_all()
        win = self.window
        #win.present()



    def renamer(self, file_chooser):
        files = self.fileChooser.get_filenames()
        for obj in files:
            holdingExtension = obj.split('.')
            extension = holdingExtension[-1]
            os.rename(obj, "new" + "." + extension)
            print("Renamed")


def main(version):
    app = Application()

    return app.run(sys.argv)
